# Helpful Hints


## Using Tools

### Vagrant

Vagrant is a VM image configuration and management tool that allow for one to configure, define, and stand up several VMs on a variety of OS's. It can be leveraged to quickly create a test cluster/sandbox for one to test and play with Ansible commands. This means, that we can create a test OS targetting our desired of, then run commands against it over and over again to ensure that our modules/roles work as expected before solidifying them into a mainline branch for others to use.

### tree

This is a linux commandline tool that you will generally have to install on fresh OS's. For the Ansible Control Server, it can be extremely helpful to orient yourself in the directory structure and determine where certain files are located, or if they exist.
---
    sudo apt-get install tree
---

### PyWinRM pip version!!

Ansible installs the lastest version of pywinrm, this is not a good thing. The version that I found (as of 2018) that worked with Ansible 2.4 and Windows 2016 server was version 0.1.1 
---
    sudo apt-get install pywinrm=0.1.1
---

### SSH Agent vs. Straight SSH

SSH Agents allow users to tie their credentials and their shell to the current agent only after logging in. This is more secure when compared to the straight ssh, where all the credentials are available to the entire system.