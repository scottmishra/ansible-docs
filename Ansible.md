# Ansible for Enterprise Utilization

## Table of Contents

1. [Intoduction](#Intro)
1. [Concerns](#Concerns)
1. [Ansible Tower](#Tower)
1. [Helpful Hints](#HelpfulHints)
1. [Ansible Certification Items)(#CertificationNeeds)]