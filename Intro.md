# Introduction

## What is Ansible 

Ansible is one of the leading Desired State Configuration Management systems (DSC) in competition with other technologies like Puppet, Chef, and Salt. Ansible's main documentation can be found at thier [Home Page](https://www.ansible.com/). The main difference between Ansible and other DSC's is that Ansible leverges a "Push" only style of configuration management. This means, unlike Puppet or Chef, that Ansible does not have a centralized node/server that the slave nodes poll to get directions from. Instead, Ansible allows any linux machine with access to the network to act as the initiator of commands that are then accomplished via SSH (or winrm for windows based nodes). This allows for a much easier/faster ramp up time to configuration management when compared to Puppet or Chef. Ansible tries to answer 4 questions that are in need of automation: Configuration, Orchestration, Automation, and Deployment. 

## Why use Ansible

Defining and storing infrastructure as code allows or all team members to have a centralized reference point for systems that they will interact with. It also allows for the VM's to be handled in a *paper plate* fashion, in that they can easily be torn down and spun up at leasure without having to worry about reconfiguration. Once familiar with Ansible syntax, there is no real limit to the size of the system or architecture that one can apply the automation too, since Ansible leverages *push* style commmands, it can be very useful for system as small as a single server, but is more than robust enough to be able to handle hundreds or thousands of servers and systems. The Ansible controls must be run from a linux based machine, but it can configure and interact with most other operating systems, so long as they offer remote connection access. Ansible packages each of its control elements inside a module, which can then be published to a public market place ([Ansible Galaxy](https://galaxy.ansible.com/))
for reuse by other developers/teams. This means that developers don't have to re-invent the wheel for several common configurations, ie: webservers, db server, and several development tools have already been automated and tested and can be used with little configuration necessary.

## Ansible Components and Data Flow

### Inventory Files

File or group of files that list the host names and/or ips. This is the ***inventory*** of the all the machines that your project in Ansible needs to know about in order send comands. Invenotry files are written in a strucutured text format, that supports grouping and variable declaration. An example of the format can be seen below:

---
    my_host ansible_ssh_host=127.0.0.1
    my_second_host ansible_ssh_host=127.0.0.2

    [datacenter]
    my_host
    my_second_host

    [datacenter:vars]
    ansible_ssh_user=my_user_name
---


### Group and Host Variables

As seen in the above inventory example, you can see that there are individual hosts and groupings of hosts that are declared in the inventory file

### Playbooks

A playbook is the collection of tasks in a single file that dictate what Ansible commands should be going where. The tasks are generally applied to a set of hosts, are named, and can have several conditions. Ansible use the other components (variables, roles, modules, etc.) to help build up playbooks to do a variety of tasks. The playbooks use a standard yaml file syntax.

Playbooks can often times get checked in to source control with bad syntax, one way to get around this is with the `--syntax-check` flag in ***ansible-playbook***. This can be a simple way to test for bad syntax before merging in changes to a main line branch.

#### Lifecycle

![Lifecycle](Lifecycle.png)

#### Tasks

##### Hosts

The basic requirement for a play and playbooks is to specify the hosts/group of hosts that the tasks are targetting. This is done with the `- hosts` tag, which takes a host pattern (series of patterns seperated by colons). With Ansible 2.4, the order of the targeted hosts can be determined using an the `order` tag.

##### Handlers

Tasks can use the `notify` tag to target a followup action that gets called when Ansible detects that the task results have ***changed*** (either on the first run of the task, or if the output/configuration has changed). Handler tasks require a globally unique name that they can be reference by. Handlers can ***listen*** to topics in order to be triggered indirectly

### Roles

### Modules

### Templates

## Key Commands

### Ad-Hoc Commands

Ansible provides the `ansible` command that can be used to initiate ad-hoc commands from the terminal. The ad-hoc commands can use anything that a play from a playbook can use, and it great for testing out commands before throwing them in a playbook. 

### Ansible Playbook

Ansible runs the collection of configuration commands knows as ***playbooks*** via the `ansible-playbook` terminal command. It is very similar to the `ansible` ad-hoc command, but it takes in the name of a playbook instead of a module/role.

### Testing

Vaidation should be built into each of the playbooks as part of stardard build out procedure. This will ensure that Ansible's fail fast methodology will work as expected and return the operator information quickly and limit the amount of rework and failures during deployment. As part of standrad build out procedure one can follow the below guidelines to help ensure sucessful maintenance of Ansible ecosystem:

- Use the same playbook all the time with embedded tests in development
- Use the playbook to deploy to a staging environment (with the same playbooks) that simulates production
- Run an integration test battery written by your QA team against staging
- Deploy to production, with the same integrated tests.
