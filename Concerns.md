# Concerns

## Windows machines

- Ansible has a lot of support for Windows servers, but they require that winrm is enabled and setup on the image before they can be controlled via Ansible. This means that the VM Image has to be created and maintained after enabling the WinRM, or that the admin must log into each Windows VM before configuring it with Ansible.

## Python 2.7 requried

- Ansible requires that the remote machine has an approiate python verion installed, this is not a problem for most linux distrobutions that come with 2.x already installed and configured, but some specialized OS versions such as ArchLinux come with python 3 installed, and this can cause issues during the automation

### Others

